const router = require("express").Router();
const User = require("../models/User");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

router.post("/register", async (req, res) => {
  const { name, email, password } = req.body;
  if (!name || !email || !password)
    res.sendStatus(400).json({ error: "All fields must be filled." });
  else {
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(password, salt);
    const user = new User({
      name,
      email,
      password: hashedPassword,
    });

    try {
      const savedUser = await user.save();
      res.send(savedUser);
    } catch (err) {
      res.sendStatus(400).send(err);
    }
  }
});

router.post("/login", async (req, res) => {
  const { email, password } = req.body;
  if (!email || !password) {
    return res.status(400).json({ error: "All fields must be filled." });
  }
  const user = await User.findOne({ email });
  if (!user) {
    return res.status(400).json({ error: "Email is not registered" });
  }
  const validPass = await bcrypt.compare(password, user.password);
  if (!validPass) {
    return res.status(400).json({ error: "Password Incorrect" });
  }
  const token = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET_KEY);
  res.header("auth-token", token).send(token);
});

module.exports = router;
