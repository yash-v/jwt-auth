const express = require("express");
const app = express();
const mongoose = require("mongoose");
require("dotenv").config();
const authRoute = require("./Routes/auth");
const postRoute = require("./Routes/posts");

mongoose.connect(process.env.MONGO_URI, () => {
  console.log(`[+] Database connected`);
});

app.use(express.json());

app.use("/api/user", authRoute);
app.use("/api/posts", postRoute);

app.listen(3000, () => console.log("Running on http://localhost:3000/"));
